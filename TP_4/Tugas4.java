import java.io.*;
import java.util.*;

class Edge {
    int from;
    int to;
    int length;
    // boolean v1 = false;
    // boolean v2 = false;

    Edge(int from, int to, int length) {
      this.from = from;
      this.to = to;
      this.length = length;
    }
}
class Graph {
    int jumVertex;
    LinkedList<Edge> [] adjList;
    ArrayList<Integer> dfsList;
    int counter;
    int combination;


    Graph(int jumVertex) {
      this.jumVertex = jumVertex;
      adjList = new LinkedList[jumVertex+1];
      for (int i = 0; i <=jumVertex ; i++) {
          adjList[i] = new LinkedList<>();
      }
    }

    void addEdge(int from, int to, int length) {
      Edge edge_1= new Edge(from, to, length);
      Edge edge_2= new Edge(to, from, length);
      adjList[from].add(edge_1);
      adjList[to].add(edge_2);
    }

    public void dfs(int vertex,int R) {
        boolean visited[] = new boolean[jumVertex+1];
        dfsList = new ArrayList<Integer>();
        if (R==0)
          dfsHelper(vertex, visited);
        else
          ccwgdHelper(vertex, visited, R);
    }
    public void dfsHelper(int from,boolean visited[]) {
        visited[from] = true;
        dfsList.add(from);
        LinkedList<Edge> list = adjList[from];
        for (int i=0; i<list.size();i++){
            if (!visited[list.get(i).to])
                dfsHelper(list.get(i).to, visited);
        }
    }
    public void ccwgdHelper(int from,boolean visited[], int R) {
        visited[from] = true;
        dfsList.add(from);
        LinkedList<Edge> list = adjList[from];
        Iterator<Edge> i = adjList[from].listIterator();
        while (i.hasNext()) {
            Edge a = i.next();
            int n = a.to;
            int m = a.length;
            if (!visited[n] && m<=R)
                ccwgdHelper(n, visited, R);
        }
    }
    public int LAOR(int kota_x,int kota_y){
      boolean visited[] = new boolean[jumVertex+1];
      int[] distance = new int[jumVertex+1];
      Queue<Integer> Q = new LinkedList<>();
      visited[kota_x] = true;
      distance[kota_x] = 0;
      Q.add(kota_x);
      while (!Q.isEmpty()) {
        int x = Q.poll();
        LinkedList<Edge> list = adjList[x];
        for (int i=0; i<list.size(); i++) {
          if (!visited[list.get(i).to]){
            // System.out.println(list.get(i).from + " " + list.get(i).to);
            distance[list.get(i).to] = distance[x] + 1;
            visited[list.get(i).to] = true;
            if (list.get(i).to == kota_y) {
              return distance[list.get(i).to];
            }
            Q.add(list.get(i).to);
          }
        }
      }
      return -1;
    }
    public int LAORC(int kota_x, int kota_y) {
        int[] dist = new int[jumVertex+1];
        int [] paths = new int[jumVertex+1];
        for (int i = 0; i < jumVertex; i++){
          dist[i] = Integer.MAX_VALUE;
        }
        BFS(kota_x, dist, paths);
        return paths[kota_y]%10001;
    }
    public void BFS(int src, int[] dist, int[] paths){
      boolean[] visited = new boolean[jumVertex+1];
      dist[src] = 0;
      paths[src] = 1;
      visited[src] = true;

      Queue<Integer> Q = new LinkedList<>();
      Q.add(src);
      while (!Q.isEmpty()) {
        int curr = Q.poll();
        LinkedList<Edge> list = adjList[curr];

        for (int i=0; i<list.size(); i++) {
          if (!visited[list.get(i).to]){
            dist[list.get(i).to] = dist[curr] + 1;
            // paths[list.get(i).to] = paths[curr];
            visited[list.get(i).to] = true;
            Q.add(list.get(i).to);
          }if (dist[list.get(i).to] > dist[curr] + 1){
            dist[list.get(i).to] = dist[curr] + 1;
            paths[list.get(i).to] = paths[curr];
          }
          else if(dist[list.get(i).to] == dist[curr] + 1){
            paths[list.get(i).to] += paths[curr];
          }
        }
      }
    }

    public int SR(int from, int to) {
      int[] table = dijkstra(from);
      if (table[to] == Integer.MAX_VALUE){
        return -1;
      }else{

        return table[to];
      }

    }

    public int[] dijkstra(int from){
      int[] value = new int[jumVertex + 1];
      for (int i = 1; i < value.length; i++) {
        value[i] = Integer.MAX_VALUE;
      }
      LinkedList<Edge> list;

      value[from] = 0;

      Queue<Integer> Q = new LinkedList<>();
      Q.add(from);

      while(!Q.isEmpty()) {
        int current = Q.poll();
        list = adjList[current];
        for(Edge edge : list) {
          int newVal = value[current] + edge.length;
          if (newVal < value[edge.to]) {
            value[edge.to] = newVal;
            Q.add(edge.to);
          }
        }
      }

      return value;
    }
    public int SM(int kota_x, int kota_y){
      int[] dijk_1 = dijkstra(kota_x);
      int[] dijk_2 = dijkstra(kota_y);
      SortedSet<Integer> sort = new TreeSet<>();
      for (int i=0;i<jumVertex ;i++ ) {
        if (Math.max(dijk_1[i],dijk_2[i]) != 0) {
          sort.add(Math.max(dijk_1[i],dijk_2[i]));
        }
      }
      System.out.println("ini yg pertama");
      for (int i = 0;i<jumVertex ;i++ ) {
        System.out.print(dijk_1[i] + " ");
      }
      System.out.println(" ");
      System.out.println("ini yg kedua");
      for (int i=0;i<jumVertex ;i++ ) {
        System.out.print(dijk_2[i] + " ");
      }
      System.out.println(" ");
      if (sort.first()!=Integer.MAX_VALUE) {
        return sort.first();
      }
      return -1;
    }

    void printGraph(){
      for (int i = 0; i <jumVertex ; i++) {
        LinkedList<Edge> list = adjList[i];
        for (int j = 0; j <list.size() ; j++) {
          System.out.println("vertex-" + i + " is connected to " + list.get(j).to +
           " with length " +  list.get(j).length);
                    }
                }
            }
        }
public class Tugas4{
  private static InputReader in;
  private static PrintWriter out;
  private static BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
  private static Set<Integer> store = new HashSet<Integer>();



  public static void main(String[] args) throws IOException {
      InputStream inputStream = System.in;
      in = new InputReader(inputStream);
      OutputStream outputstream = System.out;
      out = new PrintWriter(outputstream);

      readInput();
      out.close();
      // bw.flush();
      // bw.close();
  }
  static class InputReader {
      public BufferedReader reader;
      public StringTokenizer tokenizer;


      public InputReader(InputStream stream) {
          reader = new BufferedReader(new InputStreamReader(stream), 32768);
          tokenizer = null;
      }

      public String next() {
          while (tokenizer == null || !tokenizer.hasMoreTokens()) {
              try {
                  tokenizer = new StringTokenizer(reader.readLine());
              } catch (IOException e) {
                  throw new RuntimeException(e);
              }
          }
          return tokenizer.nextToken();
      }

      public int nextInt() {
          return Integer.parseInt(next());
      }

      public String nextLine() throws IOException{
          return reader.readLine();
      }


  }
  private static void printOutput(int answer) throws IOException {
      out.println(answer);
  }

  private static void readInput() throws IOException{
    String[] input = in.nextLine().split(" ");
    int N = Integer.parseInt(input[0]);
    int M = Integer.parseInt(input[1]);
    int Q = Integer.parseInt(input[2]);
    Graph graph = new Graph(N);
    for (int i=0;i<M ;i++ ) {
      String[] graphInput = in.nextLine().split(" ");
      graph.addEdge(Integer.parseInt(graphInput[0]),
      Integer.parseInt(graphInput[1]),Integer.parseInt(graphInput[2]));
    }
    // graph.getVertexTo(3);
    graph.printGraph();
    for (int i=0;i<Q ;i++ ) {
      String[] command = in.nextLine().split(" ");
      if (command[0].equals("OS")){
        for (int j=1;j<command.length ;j++ ) {
          graph.dfs(Integer.parseInt(command[j]),0);
          // System.out.println(graph.dfsList);
          store.addAll(graph.dfsList);
        }out.println(store.size());
        store.clear();
      }else if(command[0].equals("CCWGD")){
        graph.dfs(Integer.parseInt(command[1]),Integer.parseInt(command[2]));
        store.addAll(graph.dfsList);
        out.println(store.size() - 1);
        store.clear();
      }else if(command[0].equals("LAOR")){
        out.println(graph.LAOR(Integer.parseInt(command[1]),Integer.parseInt(command[2])));
      }else if(command[0].equals("LAORC")){
        out.println(graph.LAORC(Integer.parseInt(command[1]),Integer.parseInt(command[2])));
      }else if(command[0].equals("SR")){
        out.println(graph.SR(Integer.parseInt(command[1]),Integer.parseInt(command[2])));
      }else if(command[0].equals("SM")){
        out.println(graph.SM(Integer.parseInt(command[1]),Integer.parseInt(command[2])));
      }
    }


  }



}
