import java.io.*;
import java.util.*;
import java.lang.*;

class Node {
    // Node[] child;
    Node parent;
    ArrayList<Node> child;
    String nama;
    int level;
    String kandidat_1;
    String kandidat_2;
    long value_1;
    long value_2;

    Node(String nama){
      this.parent = null;
      this.nama = nama;
      this.level = 0;
      child = new ArrayList<Node>();
    }
    Node(String nama, Node parent){
      this.parent = parent;
      this.nama = nama;
      this.level = parent.level + 1;
      child = new ArrayList<Node>();
    }
    void addCandidate(String donat_1, String donat_2){
      this.kandidat_1 = donat_1;
      this.kandidat_2 = donat_2;
    }
    void addValue(long a, long b){
      this.value_1 += a;
      this.value_2 += b;
    }
    void subsValue(long a, long b){
      this.value_1 -= a;
      this.value_2 -= b;
    }

}

public class Tugas3{
  private static InputReader in;
  private static PrintWriter out;
  public static ArrayList<Node> nodeList = new ArrayList<Node>();
  public static Map<String,Node> nodeMap = new HashMap();
  private static BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));


  public static void main(String[] args) throws IOException {
      InputStream inputStream = System.in;
      in = new InputReader(inputStream);
      // OutputStream outputstream = System.out;
      // out = new PrintWriter(outputstream);

      readInput();
      // out.close();
      bw.flush();
      bw.close();
  }
  static class InputReader {
      public BufferedReader reader;
      public StringTokenizer tokenizer;


      public InputReader(InputStream stream) {
          reader = new BufferedReader(new InputStreamReader(stream), 32768);
          tokenizer = null;
      }

      public String next() {
          while (tokenizer == null || !tokenizer.hasMoreTokens()) {
              try {
                  tokenizer = new StringTokenizer(reader.readLine());
              } catch (IOException e) {
                  throw new RuntimeException(e);
              }
          }
          return tokenizer.nextToken();
      }

      public int nextInt() {
          return Integer.parseInt(next());
      }

      public String nextLine() throws IOException{
          return reader.readLine();
      }


  }
  private static void printOutput(int answer) throws IOException {
      out.println(answer);
  }
  public static void sumAdd(Node wilayah, long value_a, long value_b){
      if (wilayah.parent==null){
        wilayah.addValue(value_a,value_b);
        return;
      }else{
        wilayah.addValue(value_a,value_b);
        sumAdd(wilayah.parent, value_a,value_b);
      }
  }
  public static void sumSubs(Node wilayah, long value_a, long value_b){
    if (wilayah.parent==null){
      wilayah.subsValue(value_a,value_b);
      return;
    }else{
      wilayah.subsValue(value_a,value_b);
      sumSubs(wilayah.parent, value_a,value_b);
    }
  }
  public static String cekSuara(Node wilayah){
    return (wilayah.value_1+ " " + wilayah.value_2);
  }
  public static Long wilayahMenang_1(){
    long counter = 0;
    for (int i=0; i<nodeList.size() ; i++ ) {
      if (nodeList.get(i).value_1 > nodeList.get(i).value_2)
        counter++;
    }
    return counter;
  }
  public static Long wilayahMenang_2(){
    long counter = 0;
    for (int i=0; i<nodeList.size() ; i++ ) {
      if (nodeList.get(i).value_2 > nodeList.get(i).value_1)
        counter++;
    }
    return counter;
  }
  public static void suaraProvinsi(){
    for (int i = 0;i<nodeList.size() ;i++ ) {
      if (nodeList.get(i).level == 1) {
        try {
          bw.write(nodeList.get(i).nama + " " + nodeList.get(i).value_1 + " " + nodeList.get(i).value_2);
          bw.write("\n");
        }catch(IOException e) {
          // e.printStackTrace();
        }
      }
    }
  }
  public static Long wilayahMinimal_1(Long persen){
    long counter = 0;
    for (int i=0; i<nodeList.size() ; i++ ) {
      if (nodeList.get(i).value_1 + nodeList.get(i).value_2 != 0) {
        // System.out.println(((nodeList.get(i).value_1 * 100) / (nodeList.get(i).value_1 + nodeList.get(i).value_2)));
        if (((nodeList.get(i).value_1 * 100) / (nodeList.get(i).value_1 + nodeList.get(i).value_2)) >= persen)
          counter++;
      }else{
        if (persen<=50)
          counter++;
      }
    }
    return counter;
  }
  public static Long wilayahMinimal_2(Long persen){
    long counter = 0;
    for (int i=0; i<nodeList.size() ; i++ ) {
      if (nodeList.get(i).value_1 + nodeList.get(i).value_2 != 0) {
        if (((nodeList.get(i).value_2 * 100) / (nodeList.get(i).value_1 + nodeList.get(i).value_2)) >= persen)
        counter++;
      }else{
        if (persen<=50)
          counter++;
      }
    }
    return counter;
  }
  public static Long wilayahSelisih(Long selisih){
    long counter = 0;
    for (int i=0; i<nodeList.size() ; i++ ) {
      if (nodeList.get(i).value_1 - nodeList.get(i).value_2 >= selisih ||
      nodeList.get(i).value_2 - nodeList.get(i).value_1 >= selisih){
        counter++;
      }
    }
    return counter;
  }
  private static void readInput() throws IOException{
    String[] tempB = in.nextLine().split(" ");
    String donat_1 = tempB[0];
    String donat_2 = tempB[1];
    Node temp;
    int N = in.nextInt();
    for (int i=0;i<N;i++) {
      String[] inputWilayah = in.nextLine().split(" ");
      int banyakSub = Integer.parseInt(inputWilayah[1]);
      if (nodeList.size() == 0) {
        Node wilayah = new Node(inputWilayah[0]);
        wilayah.addCandidate(donat_1,donat_2);
        nodeList.add(wilayah);
        nodeMap.put(inputWilayah[0],wilayah);
        for (int j=1;j<=banyakSub;j++) {
          Node subWilayah = new Node(inputWilayah[j+1],wilayah);
          subWilayah.addCandidate(donat_1,donat_2);
          wilayah.child.add(subWilayah);
          nodeList.add(subWilayah);
          nodeMap.put(inputWilayah[j+1],subWilayah);
        }
      }else{
        Node wilayah = nodeMap.get(inputWilayah[0]);
        for (int j = 1;j<=banyakSub ; j++ ) {
          Node subWilayah = new Node(inputWilayah[j+1],wilayah);
          subWilayah.addCandidate(donat_1,donat_2);
          nodeList.get(nodeList.indexOf(wilayah)).child.add(subWilayah);
          nodeList.add(subWilayah);
          nodeMap.put(inputWilayah[j+1],subWilayah);
        }
      }
    }
    int M = in.nextInt();
    for (int i=0;i<M ;i++ ) {
      String[] tempC = in.nextLine().split(" ");
      String command = tempC[0];
      if (command.equals("TAMBAH")) {
        Node id = nodeMap.get(tempC[1]);
        sumAdd(id,Long.parseLong(tempC[2]),Long.parseLong(tempC[3]));
      }else if (command.equals("ANULIR")) {
        Node id = nodeMap.get(tempC[1]);
        sumSubs(id,Long.parseLong(tempC[2]),Long.parseLong(tempC[3]));
      }else if(command.equals("CEK_SUARA")){
        Node id = nodeMap.get(tempC[1]);
        bw.write(cekSuara(id) + "");
        bw.write("\n");
      }else if(command.equals("WILAYAH_MENANG")){
        if (nodeList.get(0).kandidat_1.equals(tempC[1])){
          bw.write(wilayahMenang_1() + "");
        }else{
          bw.write(wilayahMenang_2() + "");
        }
        bw.write("\n");
      }else if(command.equals("CEK_SUARA_PROVINSI")){
        suaraProvinsi();
      }else if(command.equals("WILAYAH_MINIMAL")){
        if (nodeList.get(0).kandidat_1.equals(tempC[1])){
          bw.write(wilayahMinimal_1(Long.parseLong(tempC[2])) + "");
        }else{
          bw.write(wilayahMinimal_2(Long.parseLong(tempC[2])) + "");
        }
        bw.write("\n");
      }else if(command.equals("WILAYAH_SELISIH")){
        bw.write(wilayahSelisih(Long.parseLong(tempC[1])) + "");
        bw.write("\n");
      }

    }

    }
}
