import java.io.*;
import java.util.*;

public class Tugas2{
  private static InputReader in;
  private static PrintWriter out;
  private static ArrayList<LinkedList<Integer>> rak_donat = new ArrayList<>();
  private static BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
  //private static LinkedList<Integer> barisDonat = new LinkedList<Integer>();

  public static void main(String[] args) throws IOException {
      InputStream inputStream = System.in;
      in = new InputReader(inputStream);

      readInput();
      bw.flush();
      bw.close();
  }
  static class InputReader {
      public BufferedReader reader;
      public StringTokenizer tokenizer;

      public InputReader(InputStream stream) {
          reader = new BufferedReader(new InputStreamReader(stream), 32768);
          tokenizer = null;
      }

      public String next() {
          while (tokenizer == null || !tokenizer.hasMoreTokens()) {
              try {
                  tokenizer = new StringTokenizer(reader.readLine());
              } catch (IOException e) {
                  throw new RuntimeException(e);
              }
          }
          return tokenizer.nextToken();
      }

      public int nextInt() {
          return Integer.parseInt(next());
      }

      public String nextLine() throws IOException{
          return reader.readLine();
      }


  }

  private static void printOutput(int answer) throws IOException {
      out.println(answer);
  }
  private static void readInput() throws IOException {
      int N = in.nextInt();
      for (int i=0;i<N;i++){
        LinkedList<Integer> barisDonat = new LinkedList<Integer>();
        String[] tempN = in.nextLine().split(" ");
        for (int j=1 ; j<Integer.parseInt(tempN[0])+1 ; j++){
          barisDonat.add(Integer.parseInt(tempN[j]));
        }
        rak_donat.add(barisDonat);
      }
      int  Q = in.nextInt();
      for (int i = 0; i < Q; i++){
        LinkedList<Integer> tempDonat = new LinkedList<Integer>();
        LinkedList<Integer> tempMove = new LinkedList<Integer>();
        String[] tempQ = in.nextLine().split(" ");
        String type = tempQ[0];
        if (tempQ.length == 3){
          int chips = Integer.parseInt(tempQ[1]);
          int barisan = Integer.parseInt(tempQ[2]);
          if (type.equals("IN_FRONT")){
            tempDonat = rak_donat.get(barisan-1);
            tempDonat.addFirst(chips);
            insertionSort(rak_donat);
          }
          else if (type.equals("IN_BACK")){
            tempDonat = rak_donat.get(barisan-1);
            tempDonat.addLast(chips);
            insertionSort(rak_donat);

          }
          else if (type.equals("MOVE_FRONT")){
            tempDonat = rak_donat.get(barisan-1);
            tempMove = rak_donat.remove(chips-1);
            for (int a = tempMove.size()-1 ; a>=0 ; a--){
              tempDonat.addFirst(tempMove.get(a));
            }
            insertionSort(rak_donat);

          }
          else if (type.equals("MOVE_BACK")){
            tempDonat = rak_donat.get(barisan-1);
            tempMove = rak_donat.remove(chips-1);
            for (int a = 0 ; a<=tempMove.size()-1 ; a++){
              tempDonat.addLast(tempMove.get(a));
            }
            insertionSort(rak_donat);

          }
        }else if (tempQ.length == 2){
          int barisan = Integer.parseInt(tempQ[1]);
          if(type.equals("OUT_FRONT")){
            tempDonat = rak_donat.get(barisan-1);
            tempDonat.removeFirst();
            if (tempDonat.size() == 0) {
              rak_donat.remove(barisan-1);
            }
            insertionSort(rak_donat);

          }
          else if (type.equals("OUT_BACK")){
            tempDonat = rak_donat.get(barisan-1);
            tempDonat.removeLast();
            if (tempDonat.size() == 0) {
              rak_donat.remove(barisan-1);
            }
            insertionSort(rak_donat);

          }
          else if (type.equals("NEW")){
            tempDonat.add(barisan);
            rak_donat.add(0,tempDonat);
            insertionSort(rak_donat);

          }
        }
      }
      for (int a=0; a<rak_donat.size(); a++){
        LinkedList<Integer> tempDonat = new LinkedList<Integer>();
        tempDonat = rak_donat.get(a);
        for (int i = 0; i<tempDonat.size(); i++) {
          bw.write(tempDonat.get(i) + " ");
        }
        bw.write("\n");
      }
  }

  public static int compare(LinkedList<Integer> baris_1, LinkedList<Integer> baris_2){
    int size1 = baris_1.size();
    int size2 = baris_2.size();
    int counter = 0;

    if (size1 >= size2){
      while (counter < size2){
        if (baris_1.get(counter) > baris_2.get(counter)){
          return 1;
        }else if (baris_1.get(counter) < baris_2.get(counter)){
          return -1;
        }else if (baris_1.get(counter).equals(baris_2.get(counter))){
          counter++;
        }
      }
      if (counter == size2)
        return 1;
    }
    if (size1 <= size2){
      while (counter < size1){
        if (baris_1.get(counter) > baris_2.get(counter)){
          return 1;
        }else if (baris_1.get(counter) < baris_2.get(counter)){
          return -1;
        }else if (baris_1.get(counter).equals(baris_2.get(counter))){
          counter++;
        }
      }
      if (counter == size1)
        return -1;
    }return 0;
  }
  public static void insertionSort(ArrayList<LinkedList<Integer>> rak_donat){
    for (int i = 1; i<rak_donat.size(); i++){
      LinkedList<Integer> temp = new LinkedList<Integer>();
      temp = rak_donat.get(i);
      int j = i;
      while ((j>0) && (compare(temp,rak_donat.get(j-1))) == -1){
        rak_donat.set(j,rak_donat.get(j-1));
        j--;
      }
      rak_donat.set(j,temp);

    }

  }






}
