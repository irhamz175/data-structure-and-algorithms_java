import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.ArrayList;

//collaborator : Syams Ramadhan 1806205413
//saya membuat input untuk restock dan transfer
//menggunakan ide dari syams

public class Tugas1{
  private static InputReader in;
  private static PrintWriter out;
  private static Map<String,Map<String,int[]>> map_toko = new HashMap();
  private static ArrayList<int[]> list_donat = new ArrayList<>();
  private static ArrayList<Integer> hasil = new ArrayList<>();
  private static Map<String,Integer> map_size = new HashMap();

  public static void main(String[] args) throws IOException {
      InputStream inputStream = System.in;
      in = new InputReader(inputStream);
      OutputStream outputStream = System.out;
      out = new PrintWriter(outputStream);

      readInput();
      out.close();
  }

  private static void printOutput(int answer) throws IOException {
      out.println(answer);
  }

  static class InputReader {
      // taken from https://codeforces.com/submissions/Petr
      public BufferedReader reader;
      public StringTokenizer tokenizer;

      public InputReader(InputStream stream) {
          reader = new BufferedReader(new InputStreamReader(stream), 32768);
          tokenizer = null;
      }

      public String next() {
          while (tokenizer == null || !tokenizer.hasMoreTokens()) {
              try {
                  tokenizer = new StringTokenizer(reader.readLine());
              } catch (IOException e) {
                  throw new RuntimeException(e);
              }
          }
          return tokenizer.nextToken();
      }

      public int nextInt() {
          return Integer.parseInt(next());
      }

      public String nextLine() throws IOException{
          return reader.readLine();
      }


  }

  private static void readInput() throws IOException{
    int N =in.nextInt();
    if (N<=20 & N>=1){
      for (int i = 0; i<N; i++ ){
        String[] nContain = in.nextLine().split(" ");
        String P = nContain[0];
        int D = Integer.parseInt(nContain[1]);
        Map<String,int[]> PnD = new HashMap();
        if (D<=20 & D>=1){
          for (int j=0;j<D;j++ ) {
            String[] dContain = in.nextLine().split(" ");
            String S = dContain[0];
            int[] JC = {Integer.parseInt(dContain[1]),Integer.parseInt(dContain[2])};
            if (JC[0]<= 100 & JC[1] <=100){
              PnD.put(S,JC);
            }
          }
          map_toko.put(P,PnD);
        }
      }
    }
    int Q = in.nextInt();
    list_donat.clear();
    if (Q<= 10 & Q>= 1){
      for(int i = 0 ;i<Q;i++){
        int X = in.nextInt();
        if(X<=N & X>=1){
          String[] tokoday = in.nextLine().split(" ");
          for(int j=0;j<X;j++){
            if (map_toko.containsKey(tokoday[j])){
              for(String key : map_toko.get(tokoday[j]).keySet()){
                list_donat.add(map_toko.get(tokoday[j]).get(key));
            }
          }
        }
      }
      String[] tContain = in.nextLine().split(" ");
      int Target = Integer.parseInt(tContain[1]);

      validity(list_donat,Target);
      int modhasil = 1000000007;
      hasil.add(map_size.size()%modhasil);
      map_size.clear();

      String[] duarString =  in.nextLine().split(" ");
      int Duar = Integer.parseInt(duarString[1]);
      for (int a = 0;a<Duar ;a++ ) {
        String[] PSB = in.nextLine().split(" ");
        String P = PSB[0];
        String S = PSB[1];
        int B = Integer.parseInt(PSB[2]);
        if(map_toko.containsKey(P)){
          Map<String,int[]> map_temp = map_toko.get(P);
          if(map_temp.containsKey(S)){
            map_temp.get(S)[0] -= B;
          }
        }
      }

      String[] restockString = in.nextLine().split(" ");
      int Restock = Integer.parseInt(restockString[1]);
      for(int j = 0;j<Restock;j++){
        String[] PSBC = in.nextLine().split(" ");
        String P = PSBC[0];
        String S = PSBC[1];
        int B = Integer.parseInt(PSBC[2]);
        int C = Integer.parseInt(PSBC[3]);
        if(map_toko.containsKey(P)){
          Map<String,int[]> map_temp = map_toko.get(P);
          if(map_temp.containsKey(S)){
            if(map_temp.get(S)[1] == C){
              map_temp.get(S)[0] += B;
            }
          }else{
            int[] int_temp = {B,C};
            map_temp.put(S,int_temp);
          }
        }
      }
      String[] temp_transfer = in.nextLine().split(" ");
      int transfer = Integer.parseInt(temp_transfer[1]);
      for(int j = 0;j<transfer;j++){
        String[] PZSB= in.nextLine().split(" ");
        String P = PZSB[0];
        String Z = PZSB[1];
        String S = PZSB[2];
        int B = Integer.parseInt(PZSB[3]);
        if(map_toko.containsKey(P) & map_toko.containsKey(Z)){
          Map<String,int[]> map_transferer = map_toko.get(P);
          Map<String,int[]> map_transfered = map_toko.get(Z);
          if(map_transferer.containsKey(S) & map_transfered.containsKey(S)){
            if(map_transferer.get(S)[1] == map_transfered.get(S)[1]){
              map_transferer.get(S)[0] -= B;
              map_transfered.get(S)[0] += B;
            }
          }else{
            map_transferer.get(S)[0] -= B;
            int[] tem = {B,map_transferer.get(S)[1]};
            map_transfered.put(S,tem);
        }
      }
    }
    }for(int h : hasil){
      System.out.println(h);
  }
 }
 }
 private static void validity(ArrayList<int[]> list_donat,int target){

   if (target <=100){
     if(target==0){
       String str = "";
       for(int[] k : list_donat){
         str += k[0] + " " + k[1] + " ";
       }
       map_size.put(str,0);
     }
     for(int i = 0;i<list_donat.size() & target>0;i++){
       if(list_donat.get(i)[0] != 0){
         int temp = list_donat.get(i)[1];
         list_donat.get(i)[0] -= 1;
         validity(list_donat,target-temp);
         list_donat.get(i)[0] += 1;
       }
     }

   }
 }
}
